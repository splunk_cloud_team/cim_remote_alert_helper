## Minimal set of standard modules to import
# import csv  # # Result set is in CSV format
# import gzip  # # Result set is gzipped
# import json  # # Payload comes in JSON format
# import logging  # # For specifying log levels
import sys  # # For appending the library path
# import time  # # For rate limiting
# import urllib.request, urllib.parse, urllib.error  # # For url encoding

## Standard modules specific to this action
# import requests  # # For making http based API calls
## Importing the cim_actions.py library
## A.  Import make_splunkhome_path
## B.  Append your library path to sys.path
## C.  Import ModularAction from cim_actions
## D.  Import ModularActionTimer from cim_actions
from splunk.clilib.bundle_paths import make_splunkhome_path

sys.path.append(make_splunkhome_path(["etc", "apps", "Splunk_SA_CIM", "lib"]))
try:
    from cim_actions import ModularAction
except ImportError:
    print("FATAL Splunk_SA_CIM 4.12 or above required", file=sys.stderr)
    sys.exit(5)

# import our helper procedure
# from TA-remote_alert_helper
sys.path.append(make_splunkhome_path(["etc", "apps", "TA-remote_alert_helper", "lib"]))
try:
    from alert_action import alert_action_helper
except ImportError:
    print("FATAL TA-remote_alert_helper required", file=sys.stderr)
    sys.exit(5)


## Set the name of this action
ACTION_NAME = "bmo_test"

## Retrieve a logging instance from ModularAction
## It is required that this endswith _modalert
logger = ModularAction.setup_logger('{}_modalert'.format(ACTION_NAME))

## Subclass ModularAction for purposes of implementing
## a script specific dowork() method
class ThisModularAction(ModularAction):

    # list of params that need to be validated
    REQUIRED_PARAMS = []

    ## This method will initialize ModularAction
    def __init__(self, settings, logger, action_name=None):
        ## Call ModularAction.__init__
        super(ThisModularAction, self).__init__(settings, logger, action_name)
        ## Initialize param.limit
        try:
            self.limit = int(self.configuration.get('limit', 1))
            if self.limit<1 or self.limit>30:
                self.limit = 30
        except:
            self.limit = 1

    ## This method will handle validation
    def validate(self, result):
        ## outer validation
        if len(self.rids)<=1:
            for param in ThisModularAction.REQUIRED_PARAMS:
                if not self.configuration.get(param):
                    raise Exception('Required Parameter {} Not Specified'.format(param))
                
            # ## Validate param.<fields> as required
            # if not self.configuration.get('phantom_server'):
            #     raise Exception('No Phantom Server Specified')
            # if not self.configuration.get('phantom_label'):
            #     raise Exception('No Phantom Label Specified')

            # ## Validate param.parameter_field
            # if self.configuration.get('parameter_field', '') not in result:
            #     raise Exception('Parameter field does not exist in result')

    ## This method will do the actual work itself
    def dowork(self, result):
        
        print("I'm doing work!!".encode("utf-8").decode("utf-8"), file=sys.stderr)

if __name__ == "__main__":
    
    alert_action_helper(sys.argv, ThisModularAction(sys.stdin.read(), logger, action_name=ACTION_NAME))

