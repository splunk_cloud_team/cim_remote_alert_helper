# Log event action settings

action.alert_template = [0|1]
* Enable alert template action

action.alert_template.param.event = <string>
* Event content sent to the receiver endpoint, which is eventually indexed

action.alert_template.param.host = <string>
* Field value of the host field of the newly indexed event

action.alert_template.param.source = <string>
* Field value of the source field of the newly indexed event

action.alert_template.param.sourcetype = <string>
* Field value of the sourcetype field of the newly indexed event

action.alert_template.param.index = <string>
* Destination index of the newly indexed event
