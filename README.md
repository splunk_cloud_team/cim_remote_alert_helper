** Instructions **

For general documentation see:
https://docs.splunk.com/Documentation/CIM/latest/User/UsetheCAM

1. Install Splunk_SA_CIM https://splunkbase.splunk.com/app/1621/
    * On the search head that will run the search/alert
    * On the remote instance(s) that will execute the action
2. Configure the Search Head API Keys
    * Run the following command on each remote instance and make note of the results:
    
        | rest /services/server/info | table serverName
        
    * Open the following URL and capture the results. You will need one key for each remote instance.
    
        https://<stackname>.splunkcloud.com/en-US/splunkd/__raw/alerts/modaction_queue/key
        
        reference: https://docs.splunk.com/Documentation/ES/latest/Admin/Adaptiveresponserelay#Configure_your_Splunk_Cloud_ES_search_head_with_an_API_key
    
    * Go to Manage Apps --> Splunk_SA_CIM --> set up
        * if the above fails to load then go to the url https://<stackname>.splunkcloud.com/en-US/app/search/cim_setup
        * from the API Key panel, add each remote instance using the names and keys from the previous steps
        
3. Configure the remote instance(s)

    * Go to Manage Apps --> Splunk_SA_CIM --> set up
        * from the API Key panel, add the key for this remote instance using the same key from the SH configuration
    
    * Create the modular input to poll the remote search head for queued alerts
    
4. Role Capabilities
    * make sure the user running the search has "edit_cam_queue" capability on the search head
5. Add entries to cam_worker_lookup.csv for the new remote workers.
    
6. Install the TA-remote-alert-helper app (no configuration required) -- on cloud this will not pass SSAI vetting, so it must be installed by puppet currently.
7. Install the remote_alert_template app (provides the actual alert action) -- this will pass SSAI vetting without issue, however, customized alerts may contain code that requires review.
8. Test
    * create an alert using makeresults or similar
    * set the alert action to alert_template
    * set the workers to "remote" or whatever you named the worker set in the lookup file (should be a dropdown)
    * if the workder set is "local" it will run on the search head
    * if the worker is not "local" the event will be queued until the remote pulls the events via the modular input, then run on the remote
    * it logs to _internal, and cim_modactions -- you should be able to watch it progress from one state to another.