# -*- coding: utf-8 -*-
## Minimal set of standard modules to import
import csv  # # Result set is in CSV format
import gzip  # # Result set is gzipped
import json  # # Payload comes in JSON format
import logging  # # For specifying log levels
import sys  # # For appending the library path
import time  # # For rate limiting

## Importing the cim_actions.py library
## A.  Import make_splunkhome_path
## B.  Append your library path to sys.path
## C.  Import ModularAction from cim_actions
## D.  Import ModularActionTimer from cim_actions
from splunk.clilib.bundle_paths import make_splunkhome_path

# import our cim_actions.py library
# from Splunk_SA_CIM
sys.path.append(make_splunkhome_path(["etc", "apps", "Splunk_SA_CIM", "lib"]))
try:
    from cim_actions import ModularActionTimer
    
except ImportError:
    print("FATAL Splunk_SA_CIM 4.12 or above required", file=sys.stderr)
    sys.exit(5)

def alert_action_helper(args, modaction):
    ## This function exists so that code requiring manual_check
    ## can be vetted once, and leveraged by multiple alert actions
    ## which can be uploaded via SSAI.
    
    ## This is standard chrome for validating that
    ## the script is being executed by splunkd accordingly
    if len(args) < 2 or args[1] != "--execute":
        print("FATAL Unsupported execution mode (expected --execute flag)", file=sys.stderr)
        sys.exit(1)

    ## The entire execution is wrapped in an outer try/except
    try:

        modaction.logger.debug(modaction.settings)
        
        ## NEW FOR CIM 4.12 
        ## Call our queuework method() to support distributed AR actions
        modaction.queuework()

        ## Add a duration message for the "alert_action_helper" component using modaction.start_timer as
        ## the start time
        with ModularActionTimer(modaction, 'alert_action_helper', modaction.start_timer):
            ## Process the result set by opening results_file with gzip
            with gzip.open(modaction.results_file, 'rt', encoding="utf-8") as fh:
                ## Iterate the result set using a dictionary reader
                ## We also use enumerate which provides "num" which
                ## can be used as the result ID (rid)
                for num, result in enumerate(csv.DictReader(fh)):
                    ## results limiting
                    if num>=modaction.limit:
                        break
                    ## Set rid to row # (0->n) if unset
                    result.setdefault('rid', str(num))
                    ## Update the ModularAction instance
                    ## with the current result.  This sets
                    ## orig_sid/rid/orig_rid accordingly.
                    modaction.update(result)
                    ## Generate an invocation message for each result.
                    ## Tells splunkd that we are about to perform the action
                    ## on said result.
                    modaction.invoke()
                    ## Validate the invocation
                    modaction.validate(result)
                    ## This is where we do the actual work.
                    ## You must implement this in the specific class
                    modaction.dowork(result)
                    ## rate limiting
                    time.sleep(1.6)
            
            ## Once we're done iterating the result set and making 
            ## the appropriate API calls we will write out the events
            modaction.writeevents(index='cim_modactions', source=modaction.action_name)

    ## This is standard chrome for outer exception handling
    except Exception as e:
        ## adding additional logging since adhoc search invocations do not write to stderr
        try:
            modaction.message(e, status='failure', level=logging.CRITICAL)
        except:
            modaction.logger.critical(e)
        print("ERROR: %s" % e, file=sys.stderr)
        sys.exit(3)
